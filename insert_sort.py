def insert_sort(L):
    n=len(L)

    for i in range(1,n-1):
        item=L[i]
        j=i-1
        while j>=0 and L[j]>item:
            L[j+1]=L[j]
            j=j-1
        L[j+1]=item

if __name__=="__main__":
    L=[8,1,77,12,67,5,19,4,7,10,100]
    # print("Before sort L: ",L)
    insert_sort(L)
    print("After sort L: ",L)

