""" Input : A list L where number of elements is n.
    step 1: let take a variable i and increament its value from 0 to n-1. for every value of i do the step 2
    step 2: find the index of lowest value element from L[i+1] till  l[n-1] and interchange that element with n-th index element
    step 3: List L's number is now sorted from lwest yo highest order
    -------------------------------------------------------------------------------------------------------------------------------
    step 1: let i=0
    step 2: if the value of i is greater than or equal to the value of (n-1) then got to step 11.
    step 3: let, the index of lowest number of list L is. index_min=i
    step 4: let,j=i+1
    step 5: if the number of j is greater than or equal to number of n, then move to step 9
    step 6: if L[j]<L[index_min] then move to next step, otherwise  go to step 8.
    step 7: index_min=j
    step 8: increament j,(j=j+1), then back to step 5
    step 9: if i and index_min is not equal, then L[i] and L[index_min] interchange
    step 10: increament i ,(i=i+1), go back to step 2
    step 11: list is sorted 

"""
def selection_sort(L):
    n=len(L)
    for i in range(0,n-1):
        index_min=i
        for j in range(i+1,n):
            if L[j]<L[index_min]:
                index_min=j
        if index_min !=i:
            L[i],L[index_min]=L[index_min],L[i]

if __name__== "__main__":
    L=[10,5,2,8,7]
    print("Before Sort L :",L) 
    selection_sort(L)
    print("After Sort L :",L)
