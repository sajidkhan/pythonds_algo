class BstNode():
    def __init__(self,data):
        self.left=None
        self.right=None
        self.parent=None
        self.data=None

    def __repr__(self):
        return repr(self.data)

    def add_left(self,node):
        self.left=node
        if node is not none:
            node.parent=self

    def add_right(self,node):
        self.right=node
        if node is not none:
            node.parent=self


def bst_insert(root,node):
    last_node=None
    current_node=root

    while current_node is not None:
        last_node=current_node
        if node.data < current_node.data:
            current_node.left=node
        else:
            current_node.right=node
    if last_node is None:
        last_node=root
    elif node.data<last_node.data:
        last_node.add_left(node)
    else:
        last_node.add_right(node)
    return root

def create_bst():
    root=BstNode(10)

    for item in [5,17,3,7,12,19,1,4]:
        node=BstNode(item)
        root=bst_insert(root,node)
    return root

if __name__=="__main__":
    root=create_bst()
    print(root)