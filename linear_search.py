""" input: a List L & a element x
    output: i is the index of L, where the element x is found.if not found i=-1

    step 1: let he number elements in L is n
    step 2: increment(i++) the the value of i from 0 to (n-1). For ever value of i go to step 3
    step 3: is the L[i] and x equal? If so return i.
    step 4: let i=-1
"""

""" input: a List L & a element x
    output: i is the index of L, where the element x is found.if not found i=-1

    step 1: Let number of elements in L is n
    step 2: take i=0
    step 3: if the value of i is less then n then move to step 4. otherwise move to step 7
    step 4: is the L[i] and x equal? If so move to step 5.
    step 5: return i.(result: the x is found in i )
    step 6: increament(i++) and get back to step 3
    step 7: let i=-1 and return 1 (result: the expected element x is not found )
"""
def linear_search(L,x):
    n=len(L)
    i=0
    while i<n:
        if L[i]==x:
            return i
        i+=1
    i=-1
    return -1

if __name__ == "__main__":
    L=[1,2,5,6,4,3,9,7,10]
    x=0
    result=linear_search(L,x)
    assert L[result]==x,"x not found"