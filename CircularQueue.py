class CircularQueue:
    def __init__(self,size):
        self.items=[0]*size
        self.max_size=size
        self.head,self.tail,self.size=0,0,0

    def enqueue(self,item):
        if self.is_full():
            print("The list is Full")
            return
        print("inserting Items",item)
        self.items[self.tail]=item
        self.tail=(self.tail+1)%self.max_size
        self.size+=1

    def dequeue(self):
        item=self.items[self.head]
        self.head=(self.head+1)%self.max_size
        self.size-=1
        return item

    def is_empty(self):
        if self.items==0:
            return True
        return False

    def is_full(self):
        if self.size==self.max_size:
            return True
        return False

if __name__=="__main_":
    q=CircularQueue(3)
    # print(q.items)

    q.enqueue("sajid")
    q.enqueue("mollika")
    q.enqueue("raakin")
    q.enqueue("raakin11")

    # print(q.items)
    while not q.is_empty():
        person=q.dequeue()
        print(person)