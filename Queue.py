class Queue():
    def __init__(self):
        self.list=[]

    def enqueue(self,item):
        self.list.append(item)

    def dequeue(self):
        return self.list.pop(0)
    
    def is_empty(self):
        if not self.list==[]:
            return True
        return False

if __name__=="__main__":
    q=Queue()

    q.enqueue('sajid')
    q.enqueue('mollika')
    q.enqueue('raakin')

    print(q.list)

    while not q.is_empty:
        item=q.dequeue()
        print(item)
