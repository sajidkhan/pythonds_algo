def average(L):
    if not L:
        none
    return sum(L)/len(L)

if __name__=="__main__":
    L=[1,2,3,4,5]
    expected_result=3.0
    #result=average(L)
    assert expected_result==average(L),"average() produced incorrect result"
    
    """ if expected_result==result:
        print("test passed")
    else:
        print("Test faild","received:",result,"expected:",expected_result) """