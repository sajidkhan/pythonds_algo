""" input: L is a List and x is one of L's element
    outout: i is the index of List, where x is found.if not found i=-1

    step 1: Let L has n number of elements
    step 2: let left=0 and right=n-1. We have to search x through the number of left to right index.
                If left and right is equal then the list has only one element.
                If left is greater then right then there is no element or this situation is not posible
    step 3: if left <=right then move to step 4.
    step 4: let mid=(left+right)/2. mid is the middle index of L
    step 5: if L[mid]==x then return mid
    step 6: if L[mid]>x then left=mid-1
    step 7: if L[mid]<x then left=mid+1
    step 8: if x is not found in L then return i=-1
 """
def binary_search(L,x):
    n=len(L)
    #print(n)
    left=0
    right=n-1

    while left<=right:
        mid=(left+right)//2

        if L[mid]==x:
            return mid
        if L[mid]<x:
            left=mid+1
        else:
            right=mid-1

    return -1

if __name__ == "__main__":
    L=[1,2,5,6,9,10,34,43,46,50,77,81,87,90,95,101,340,430,460,500]
    x=500
    result=binary_search(L,x)
    print(result)
    assert L[result]==x,"x not found"