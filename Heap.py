def left(i):
    return 2*i

def right(i):
    return 2*i+1
def parent(i):
    return i//2
def is_max_heap(H):
    n=len(H)-1
    for i in range(n,0,-1):
        p=parent(i)
        if H[p]<H[i]:
            return False
        return True
def heap_maxify(heap,heap_size,i):
    l=left(i)
    r=right(i)
    if l<=heap_size and heap[l]>heap[i]:
        largest=l
    else:
        largest=i
    if r<=heap_size and heap[r]>heap[largest]:
        largest=r
    if largest!=i:
        heap[i],heap[largest]=heap[largest],heap[i]
        return heap_maxify(heap,heap_size,largest)

def build_maxheap(H):
    heap_size=len(H)-1
    for i in range(heap_size//2,0,-1):
        heap_maxify(H,heap_size,i)

def heap_sort(heap):
    build_maxheap(heap)
    heap_size=len(heap)-1
    for i in range(heap_size,1,-1):
        heap[1],heap[i]=heap[i],heap[1]
        heap_size-=1
        heap_maxify(heap,heap_size,1)

if __name__=="__main__":
    H=[None, 19, 7, 17, 3, 5, 12, 10, 1, 2]
    print(H,is_max_heap(H))
    H=[None, 19, 7, 17, 3, 5, 12, 10, 1, 4]
    print(H,is_max_heap(H))
    H=[None,1, 2,3]
    print(H,is_max_heap(H))
    H=[None, 2,1,3]
    print(H,is_max_heap(H))
    H=[None,3, 1,2]
    print(H,is_max_heap(H))

    print("=========heap-maxify=========")
    H=[None, 19, 7, 12, 3, 5, 17, 10, 1, 2]
    print(H)
    print(H,heap_maxify(H,9,3))
    print()
    H=[None, 1, 2,3]
    print(H)
    print(H,heap_maxify(H,4,1))

    print("=========Build heap-max=========")
    from binarytree import heap
    heap=[None, 12, 7, 1, 3, 10,17, 19, 5, 2, 5]
    print("Before convert to Heap: " ,heap)

    build_maxheap(heap)
    print("After convert to Heap: " ,heap)

    print("=========heap sort=========")

    heap=[None, 12, 7, 1, 3, 10,17, 19, 2, 5]
    print("Before sort: " ,heap)

    build_maxheap(heap)
    print("After sort: " ,heap)



